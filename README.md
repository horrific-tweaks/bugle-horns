This datapack implements the [Goat Horn](https://minecraft.fandom.com/wiki/Goat_Horn) item from bedrock, which will drop every time a goat rams into a player. Using it will play the raid horn sound.

If the player clicks on an anvil while holding the goat horn, it will turn into a tuba or a bugle horn. Using these items will play the raid horn sound at a different pitch, which amusingly sounds a lot like someone attempting to play a tuba/bugle (badly).

While holding a tuba, if the player spams the use button and does not have a helmet equipped, the tuba may end up on the player's head.

![A player holding the tuba item](./.images/tuba.jpeg)
