---
collection: "horns"
---

<% set values = horns | list %>
item replace entity @s weapon.mainhand with minecraft:carrot_on_a_stick<< values[(index+1) % values|length].value.nbt | dump_nbt >>
playsound block.anvil.use block @a ~ ~ ~
scoreboard players set @s bugle.change 0
