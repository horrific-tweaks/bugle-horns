execute as @a if score @s bugle.notused matches ..21 run scoreboard players add @s bugle.notused 1

<% for horn, horninfo in horns %>
execute as @a[gamemode=!spectator,nbt={SelectedItem:{id:"minecraft:carrot_on_a_stick",tag:{CustomModelData:<< horninfo.nbt.CustomModelData >>}}}] if score @s bugle.used matches 1.. at @s run function fennifith:bugle_horns/use/<< horn >>
execute as @a[gamemode=!spectator,nbt={SelectedItem:{id:"minecraft:carrot_on_a_stick",tag:{CustomModelData:<< horninfo.nbt.CustomModelData >>}}}] if score @s bugle.change matches 1.. at @s run function fennifith:bugle_horns/change/<< horn >>
<% endfor %>

scoreboard players set @a bugle.used 0
scoreboard players set @a bugle.change 0
