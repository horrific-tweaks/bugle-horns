# Determines when a bugle is used
scoreboard objectives add bugle.used minecraft.used:minecraft.carrot_on_a_stick
scoreboard objectives add bugle.notused dummy

# Determines when to "change" the horn type
scoreboard objectives add bugle.change minecraft.custom:minecraft.interact_with_anvil
